
import { readFileSync } from "node:fs";
export function require(file) {
    return JSON.parse(readFileSync(file));
}

Array.prototype.shuffle = function() {
    for(let i = this.length - 1 ; i > 0 ; i--)
	{
        const j = ~~(Math.random() * (i + 1));
        [this[i], this[j]] = [this[j], this[i]];
    }
    return this;
}


String.prototype.toSnakeCase = function() {
	// Weird regexes to not accidentally turn strings like HTTPQuery into H_T_T_P_Query
	return this.replace(/(?!^)[A-Z](?=[^A-Z])/g, letter => `_${letter}`)
			.replace(/(?<=[^A-Z])[A-Z](?=[A-Z])/g, letter => `_${letter}`)
			.toUpperCase();
}
