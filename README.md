
# Gaan Lah Baas

This bot is french so if you don't speak french... well, too bad!

Potit bot de musique.


# Utilisation
Voir `package.json` pour les prérequis.

`auth.json`
```JSON
{
	"token": "le token d'authentification de votre bot",
 	"master": "votre id d'utilisateurice",
	"youtube": {
		"API_KEY": "clé API pour YouTube"
	},
}
```

# Licence
**Hun Kaal Zonn** est fourni sous Licence Publique Rien À Branler (WTFPL). Pour plus de détails, lisez COPYING.txt, ou ce lien : [http://sam.zoy.org/lprab/COPYING](http://www.wtfpl.net/txt/copying)

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)
