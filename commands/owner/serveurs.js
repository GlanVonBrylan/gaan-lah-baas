
export const description = "Affiche les serveurs ou en quitte un";
export const options = [{
	type: STRING, name: "quitte",
	description: "Le serveur à quitter",
}];
export function run(inter)
{
	const id = inter.options.getString("quitte");
	if(id)
	{
		const server = inter.client.guilds.cache.get(id);
		if(server)
			server.leave().then(() => inter.reply(`J'ai quitté ${server.name}.`));
		else
			inter.reply({flags: "Ephemeral", content: "Je ne suis pas dans ce serveur.\nVous m'avez bien donné son id ?"});
	}
	else
	{
		const guilds = inter.client.guilds.cache.map(({id, name, memberCount}) => ({name, value: `${id}\n${memberCount} membres`, inline: true}));
		const embeds = [];
		for(let i = 0 ; i < guilds.length ; i += 25)
			embeds.push({fields: guilds.slice(i, i+25)});

		embeds[0].title = `${guilds.length} serveurs`;
		inter.reply({embeds});
	}
}
