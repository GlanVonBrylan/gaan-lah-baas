
export const description = "Temps d'activité";
export function run(inter)
{
	let seconds = ~~process.uptime(),
		days    = ~~(seconds / 84400);
	seconds -= days * 84400;
	let hours   = ~~(seconds / 3600);
	seconds -= hours * 3600;
	let minutes = ~~(seconds / 60);
	seconds -= minutes * 60;

	if(hours   < 10) hours   = "0"+hours;
	if(minutes < 10) minutes = "0"+minutes;
	if(seconds < 10) seconds = "0"+seconds;

	inter.reply(`${days} jour${days > 1 ? "s" : ""}, ${hours}:${minutes}:${seconds}`);
}
