
import { client } from "../../bot.js";

export const description = "Éteint le bot";
export function run(inter)
{
	inter.reply({ flags: "Ephemeral", content: "À plus." }).catch(Function()).finally(() => {
		client.destroy();
		process.exit();
	});
}
