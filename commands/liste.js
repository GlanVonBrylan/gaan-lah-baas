
import { ButtonStyle } from "discord.js";
const { Secondary: SECONDARY } = ButtonStyle;

import { formatDuration } from "../tools/youtube.js";
import { buttons, register } from "../components.js";


export const description = "Affiche la liste de lecture";
export const options = [{
	type: SUBCOMMAND, name: "voir",
	description: "Affiche la liste de lecture",
}, {
	type: SUBCOMMAND, name: "passer",
	description: "Passer des pistes de la liste de lecture",
	options: [{
		type: INTEGER, name: "nombre",
		description: "Le nombre de pistes à passer (par défaut 1)",
		minValue: 1,
	}],
}];
export function run(inter)
{
	const { musicPlayer: player } = inter.guild;
	const queueInfo = player.queue;
	if(!queueInfo)
		return inter.reply({flags: "Ephemeral", content: "Je ne suis pas en vocal."});

	const { current, queue } = queueInfo;
	if(!current)
		return inter.reply({embeds: [{ description: "Je ne joue rien actuellement." }]});

	if(inter.options.getSubcommand() === "passer")
	{
		const n = inter.options.getInteger("nombre") || 1;
		const s = n === 1 ? "" : "s";
		queue.splice(0, n);
		const reply = queue.length ? `🎶 ${n} piste${s} passée${s}.` : "🎶 Liste de lecture vidée.";
		inter.reply({embeds: [{ description: reply }]});
	}
	else
		inter.reply(displaySlice(queueInfo));
}


const ROW_LENGTH = 50;

function displaySlice({current, queue, looping}, shift = 0)
{
	const { length } = queue;
	shift = Math.min(shift, length - length % 9);
	const totalPages = Math.ceil(length / 9);
	const curPage = shift / 9 + 1;
	const pages = `Page ${curPage}/${totalPages}`;
	const totalTime = `Total : ${formatDuration(queue.reduce((d, {duration}) => d+duration, 0))}`;
	const queueSlice = !length
		? (looping ? "*La même chose*" : "*rien*")
		: `\`\`\`nim\n${queue.slice(shift, shift + 9).map(({ title, duration }, i) => {
		duration = formatDuration(duration);
		const spaceForTitle = ROW_LENGTH - (shift >= 90 ? 5 : shift ? 4 : 3) - duration.length - 1;
		return `${i+shift+1}) ${title.length > spaceForTitle
			? title.substring(0, spaceForTitle-1) + "…"
			: title.padEnd(spaceForTitle)} ${duration}`;
	}).join("\n")}\n\n${pages.padEnd(ROW_LENGTH - totalTime.length)}${totalTime}\`\`\``;

	const components = [
		{style: SECONDARY, label: "←", customId: `${shift-9}_music_queueShift`, disabled: curPage === 1},
		{style: SECONDARY, label: "🔀", customId: `${shift}_music_shuffle`, disabled: !totalPages},
		{style: SECONDARY, label: "↺", customId: `${shift}_music_queueShift`},
		{style: SECONDARY, label: "🗑️", customId: `${shift}_music_clear`, disabled: !totalPages},
		{style: SECONDARY, label: "→", customId: `${shift+9}_music_queueShift`, disabled: !totalPages || curPage === totalPages},
	];
	for(const button of components)
		register(button, updateList, 3600);

	const { startedPlaying, duration, startAt = 0, title } = current;
	const remainingTime = startedPlaying
		? formatDuration(startedPlaying + duration - startAt - ~~(Date.now() / 1000))
		: "--:--";

	return { components: [buttons(components)], embeds: [{
		title: `🎶 ${title}`,
		description: `Temps restant : ${remainingTime}\n**À suivre :** ${queueSlice}`,
		footer: looping ? { text: "🔁 Lecture en boucle activée !" } : undefined,
	}]};
}


function updateList(inter)
{
	const { musicPlayer } = inter.guild;
	const queueInfo = musicPlayer.queue;
	if(!queueInfo)
		return inter.update({ embeds: [{description: "Je ne suis plus en vocal."}], components: [] });
	const { current, queue } = queueInfo;
	if(!current)
		return inter.update({ embeds: [{description: "Je ne joue rien actuellement."}], components: [] });

	const { customId } = inter;
	if(customId.endsWith("shuffle"))
		musicPlayer.shuffle();
	else if(customId.endsWith("clear"))
		queue.length = 0;

	inter.update(displaySlice(queueInfo, parseInt(inter.customId)));
}
