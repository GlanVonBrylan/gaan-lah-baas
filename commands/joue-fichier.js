
import { connect, MAX_DURATION, FileStreamer } from "../tools/MusicPlayer.class.js";
import { formatDuration } from "../tools/youtube.js";
const MAX_SIZE = 100 * 1048576;

import { options as playOptions, commandOptions } from "./joue.js";

export const description = "Joue un truc en vocal";
export const options = [...playOptions];
options[0] = {
    type: ATTACHMENT, name: "fichier", required: true,
    description: "Fichier audio (par exemple wav ou mp3) à jouer.",
};
export async function run(inter)
{
	const { options } = inter;
	const audio = options.getAttachment("fichier");
	const { contentType, size, url } = audio;
	if(!contentType?.startsWith("audio"))
		return inter.reply({flags: "Ephemeral", content: "Vous devez fournir un fichier audio."});
	if(!size)
		return inter.reply({flags: "Ephemeral", content: `Le fichier est vide.`});
	if(size > MAX_SIZE)
		return inter.reply({flags: "Ephemeral", content: `Le fichier ne peut pas faire plus de 100 Mio.`});

	const player = connect(inter);
	if(player.error)
		return inter.reply({flags: "Ephemeral", content: player.error});

	//await inter.deferReply();

	audio.duration ??= FileStreamer.probeDuration(url);
	if(audio.duration > MAX_DURATION)
		return inter.reply({flags: "Ephemeral", content: `Ce fichier est trop long (max : ${MAX_DURATION/60} minutes).`});

	const { body: stream } = await fetch(url);
	const { atBeginning, skip } = commandOptions(inter);
	const file = new FileStreamer(stream, audio, inter.member);
	player.addToQueue(file, !!atBeginning, skip);
	inter.reply({embeds: [{
		description: `🎶 ${file.author} a ajouté **${file.name}**${atBeginning} (${formatDuration(audio.duration)}).`,
	}]});
}