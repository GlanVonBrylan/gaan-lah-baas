
export const nameLocalizations = { fr: "mélange" };
export const description = "Mélange la liste de lecture";
export function run(inter)
{
	const { musicPlayer: player } = inter.guild;
	const { queue } = player;
	if(!queue)
		return inter.reply({flags: "Ephemeral", content: "Je ne suis pas en vocal."});
	const { queue: {length} } = queue;
	if(length < 2)
		return inter.reply({flags: "Ephemeral", content: length === 0 ? "La liste d'attente est vide." : "La liste n'a qu'un élément."});

	inter.reply({embeds: [{ description: `🎶🔀 ${randomMessage(player)}` }]});
	player.shuffle();
}

const messages = Object.freeze([
	"Et c'est parti !",
	"Touille touille !",
	"On mélange !",
	"Attention : changement de sens !",
	"Tourbilol !",
]);

const lastMsg = new WeakMap();
function randomMessage(musicPlayer)
{
	const last = lastMsg.get(musicPlayer);
	let rand = ~~(Math.random() * messages.length);
	if(rand === last)
		rand = rand === 0 ? messages.length - 1 : rand + 1;
	lastMsg.set(musicPlayer, rand);
	return messages[rand];
}