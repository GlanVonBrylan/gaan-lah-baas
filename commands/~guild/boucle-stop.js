
import { guildCommands } from "@brylan/djs-commands";
const { createCmd, deleteCmd } = guildCommands;

export const shouldCreateFor = () => false;
export const nameLocalizations = { fr: "stop-boucle" };
export const description = "Arrête de jouer en boucle.";
export function run(inter)
{
    const { guild } = inter;
    guild.musicPlayer.looping = false;
    deleteCmd("boucle-stop", guild);
    createCmd("boucle", guild, true);
	inter.reply({embeds: [{ description: "🎶🔂 On arrête de boucler." }]});
}