
import { guildCommands } from "@brylan/djs-commands";
const { createCmd, deleteCmd } = guildCommands;

//export const shouldCreateFor = () => true;
export const description = "Joue la liste en boucle. Quand une musique est finie, elle est remise à la fin !";
export function run(inter)
{
    const { guild, guild: { musicPlayer: player } } = inter;
    const { channel } = inter.member.voice;
	if(channel !== player.connected()?.channel)
	{
		const err = player.cantConnect(channel);
		if(err)
			return inter.reply({ flags: "Ephemeral", content: err });

		player.connect(channel);
    }
    
    guild.musicPlayer.looping = true;
    deleteCmd("boucle", guild);
    createCmd("boucle-stop", guild, true);
	inter.reply({embeds: [{ description: "🎶🔁 On boucle la liste !" }]});
}