
import { ButtonStyle } from "discord.js";
const { Secondary: SECONDARY } = ButtonStyle;

import { selectMenu, buttons, register } from "../components.js";
import { connect, MAX_DURATION } from "../tools/MusicPlayer.class.js";
import { search, get, error403, formatDuration } from "../tools/youtube.js";

import { commandOptions } from "./joue.js";

var searchCount = 0;

function esc(str) {
	return str.replaceAll("\"", "\\\"");
}

export const description = "Cherche une vidéo sur YouTube à jouer en vocal";
export const options = [{
	type: STRING, name: "recherche", required: true,
	description: "Les termes à rechercher",
}, {
	type: STRING, name: "en-debut-de-liste",
	description: "S'il faut mettre cette/ces piste(s) au début de la liste plutôt qu'à la fin",
	choices: [{name: "oui", value: "oui"}],
}, {
	type: NUMBER, name: "passer",
	description: "S'il faut passer des pistes immédiatement. 1 pour passer la musique en cours.",
	minValue: 1,
}];
export function run(inter)
{
	function handleErr(err) {
		inter.editReply(err.code === 403
			? error403(err)
			: `Erreur : ${err.message}`).catch(Function());
		error(err);
	}

	const player = connect(inter);
	if(player.error)
		return inter.reply({flags: "Ephemeral", content: player.error});

	const defer = inter.deferReply();
	const query = inter.options.getString("recherche");
	const { skip, atBeginning } = commandOptions(inter);
	search(query).then(async results => {
		await defer;
		if(!results.length)
			return inter.editReply({embeds: [{ description: "Aucun résultat." }]}).catch(handleErr);

		results = results.filter(({duration, ageRestricted}) => (
			duration > 1 && duration < MAX_DURATION
			&& !ageRestricted
		));
		if(!results.length)
			return inter.editReply({embeds: [{ description: "Les résultats étaient tous trop longs ou soumis à une limite d'âge." }]}).catch(handleErr);

		const customId = `musicSearch_${++searchCount}`;
		inter.editReply({
			embeds: [{
				description: `🎶 ${inter.user} cherche "${esc(query)}"`,
				footer: query.includes("youtube.com/") || query.includes("youtu.be/")
					? { text: "Si vous avez une URL de vidéo ou de playlist, utilisez plutôt /joue" }
					: undefined
			}],
			components: [
				selectMenu(customId, results.map(video => ({
					value: video.id,
					label: video.title.length <= 100 ? video.title : `${video.title.substring(0, 98)}…`,
					description: `${formatDuration(video.duration)} — ${video.channel}`.substring(0, 100),
				})), 1, results.length),
				buttons({style: SECONDARY, label: "Annuler", customId: `${customId}_cancel`}),
			],
		}).catch(handleErr);

		register(customId, inter => handleReply(player, inter, atBeginning, skip));
		register(`${customId}_cancel`, ({message}) => message.delete().catch(Function()));
	}, handleErr);
}


async function handleReply(player, interaction, atBeginning, skip)
{
	const { values } = interaction;
	const chosenSongs = (await get(values.join(",")))
		.filter(({ duration }) => duration <= MAX_DURATION);
	const chosenSongsList =
		  chosenSongs.length > 5 ? `${chosenSongs.length} pistes`
		: chosenSongs.length > 1 ? ` :\n${chosenSongs.map(({title, id}) => `[${title}](https://youtu.be/${id})`).join("\n")}`
		: `[${chosenSongs[0].title}](https://youtu.be/${chosenSongs[0].id})`;

	interaction.update({ components: [], embeds: [{
		description: `🎶 ${interaction.user} a ajouté${atBeginning} ${chosenSongsList}`,
		footer: {text: `Total : ${Math.round(chosenSongs.reduce((d, {duration}) => d+duration, 0) / 60)} minutes ${chosenSongs.length === values.length ? "" : "(certaines étaient trop longues et ont été omises)"}`},
	}]});
	
	if(atBeginning && skip > 1)
		player.skip(skip - 1, true);
	player.addToQueue(chosenSongs, atBeginning);
	if(skip)
		player.skip(skip);
}
