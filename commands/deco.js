
export const nameLocalizations = { fr: "déco" };
export const description = "Déconnecte le bot du vocal";
export function run(inter)
{
	inter.reply(inter.guild.musicPlayer.disconnect()
		? { embeds: [{ description: "🎶 Déconnecté." }] }
		: { flags: "Ephemeral", content: "Je n'étais pas connecté." }
	);
}
