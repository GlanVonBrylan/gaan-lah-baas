
import { connect, MAX_DURATION } from "../tools/MusicPlayer.class.js";
import {
	YT_URL_REGEX, getId,
	search, get, getPlaylist,
	error403,
	tToSeconds, formatDuration, getBestRes,
} from "../tools/youtube.js";

export const description = "Joue un truc en vocal";
export const options = [{
	type: STRING, name: "url", required: true,
	description: "Une URL de vidéo ou de playlist YouTube, ou les termes à rechercher (joue le 1er résultat).",
}, {
	type: STRING, name: "en-debut-de-liste",
	description: "S'il faut mettre cette/ces piste(s) au début de la liste plutôt qu'à la fin",
	choices: [{name: "oui", value: "oui"}],
}, {
	type: NUMBER, name: "passer",
	description: "S'il faut passer des pistes immédiatement. 1 pour passer la musique en cours.",
	minValue: 1,
}];
export function run(inter)
{
	const player = connect(inter);
	if(player.error)
		return inter.reply({flags: "Ephemeral", content: player.error});

	const query = inter.options.getString("url");
	if(YT_URL_REGEX.test(query))
	{
		const playlist = query.match(/list=[^&]+/);
		if(playlist)
			playList(inter, playlist[0].substring(5));
		else
			playVideo(inter);
	}
	else
		searchAndPlay(inter, query);
}


export function commandOptions(inter)
{
	return {
		player: inter.guild.musicPlayer,
		atBeginning: inter.options.getString("en-debut-de-liste") ? " en début de liste" : "",
		skip: inter.options.getNumber("passer"),
	};
}


function playVideo(inter)
{
	const url = inter.options.getString("url");
	const id = getId(url);
	if(!id)
		inter.reply({flags: "Ephemeral", content: "Je n'ai pas trouvé d'id de vidéo."});
	else
	{
		get(id).then(([video]) => {
			if(!video)
				inter.reply({flags: "Ephemeral", content: "Cette vidéo n'existe pas."});
			else if(video.duration > MAX_DURATION)
				inter.reply({flags: "Ephemeral", content: `Cette vidéo est trop longue (max : ${MAX_DURATION/60} minutes).`});
			else
			{
				const start = tToSeconds(url);
				if(start && start < video.duration)
					video.startAt = start;
				play(inter, video);
			}
		}, onError.bind(inter));
	}
}

function play(inter, video)
{
	const { player, atBeginning, skip } = commandOptions(inter);
	const { id, title, duration, startAt, thumbnails = {}, ageRestricted } = video;
	const t = startAt ? `&t=${startAt}` : "";
	let reply;
	if(ageRestricted)
	{
		reply = { flags: "Ephemeral", embeds: [{
			description: `🔞 Impossible de lire [${title}](https://youtu.be/${id}${t}), cette vidéo est soumise à une limite d’âge.`,
		}]};
	}
	else
	{
		player.addToQueue(video, !!atBeginning, skip);
		reply = { embeds: [{
			description: `🎶 ${inter.user} a ajouté${atBeginning} [${title}](https://youtu.be/${id}${t}) (${formatDuration(duration, startAt)})`,
			thumbnail: getBestRes(thumbnails),
		}]};
	}
	inter[inter.deferred ? "editReply" : "reply"](reply);
}


function playList(inter, playlistId) {
	return getPlaylist(playlistId).then(videos => {
		if(!videos.playlistInfo)
			return playVideo(inter);

		const { playlistInfo: { title } } = videos;
		const songs = videos.filter(({duration, ageRestricted}) => (
			duration <= MAX_DURATION
			&& !ageRestricted
		));
		for(const song of songs)
			song.playlist = playlistId;

		const { player, atBeginning, skip } = commandOptions(inter);
		const skipped = songs.length !== videos.length;
		const totalLength = songs.reduce((d, {duration}) => d+duration, 0);
		inter.reply({ embeds: [{
			description: `🎶 ${inter.user} a ajouté${atBeginning} ${songs.length} pistes de [${title}](https://youtube.com/playlist?list=${playlistId})`,
			footer: {text: `Total : ${Math.round(totalLength / 60)} minutes ${skipped ? "(certaines étaient trop longues et ont été omises)" : ""}`},
		}]});

		player.addToQueue(songs, !!atBeginning, skip);
	}, onError.bind(inter));
}

function searchAndPlay(inter, query) {
	return Promise.all([inter.deferReply(), search(query)]).then(([, results]) => {
		const video = results.find(({duration}) => duration <= MAX_DURATION);
		if(video)
			play(inter, video);
		else
			inter.editReply("Aucune vidéo trouvée.");
	}, onError.bind(inter));
}


function onError(err)
{
	switch(err.code)
	{
		case 403:
			this.editReply(error403(err)).catch(Function());
			break;
		case 404:
			this.reply({flags: "Ephemeral", content: "Cette playlist n'existe pas ou est privée."});
			break;
		case 500:
			this.reply({flags: "Ephemeral", content: "YouTube a planté en essayant de récupérer la playlist. Est-ce que c'est un Mix ? Je n'ai pas accès aux mix, il me faut une vraie playlist."});
			break;
		default:
			this.editReply(`Erreur : ${err.message}`).catch(Function());
			error(err);
	}
}
