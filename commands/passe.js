
export const description = "Passe la musique en cours et joue la suivante";
export const options = [{
	type: INTEGER, name: "nombre",
	description: "Le nombre de vidéos à passer",
	minValue: 1,
}];
export function run(inter)
{
	const { musicPlayer: player } = inter.guild;
	if(!player.connected())
		return inter.reply({flags: "Ephemeral", content: "Je ne suis pas en vocal."});

	const n = Math.min(inter.options.getInteger("nombre") || 1, player.queue.queue.length + 1);
	inter.reply({embeds: [{ description: n === 1 ? "🎶 Je zappe." : `🎶 ${n} pistes passées (dont celle en cours).` }]});
	player.skip(n);
}
