
export const description = "Supprime les doublons de la liste de lecture";
export function run(inter)
{
	const removed = inter.guild.musicPlayer.removeDuplicates();
	switch(removed)
	{
	case null: inter.reply({flags: "Ephemeral", content: "La liste de lecture est vide."}); break;

	case false: inter.reply({flags: "Ephemeral", content: "La liste de lecture ne contient qu'une piste."}); break;

	case 0: inter.reply({embeds: [{ description: "Il n'y avait aucun doublon." }]}); break;

	default:
		const s = removed === 1 ? "" : "s";
		inter.reply({embeds: [{ description: `${removed} doublon${s} retiré${s}.` }]});
	}
}
