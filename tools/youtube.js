
// Docs: https://developers.google.com/youtube/v3/docs
import { require } from "../utils.js";
const { youtube: { API_KEY } } = require("auth.json");
const BASE_URL = "https://youtube.googleapis.com/youtube/v3";
const SEARCH_URL = `${BASE_URL}/search?key=${API_KEY}&part=id&eventType=none&maxResults=25&type=video&videoSyndicated=true&q=`;
const GET_URL = `${BASE_URL}/videos?part=id,snippet,contentDetails&key=${API_KEY}&id=`
const GET_PLAYLIST_URL = `${BASE_URL}/playlistItems?part=contentDetails,status&maxResults=25&key=${API_KEY}&playlistId=`
const GET_PLAYLIST_INFO_URL = `${BASE_URL}/playlists?part=snippet&key=${API_KEY}&id=`


export const YT_URL_REGEX = /https?:\/\/(www\.)?(youtu\.be\/|(music\.)?youtube\.com\/(watch\?v|playlist\?list)=).+/;


export function error403({ message })
{
	return message.includes("quota")
		? "Oups, YouTube me dit que j'ai dépassé mon quota pour la journée."
		: "Erreur en voulant communiquer avec YouTube.";
}


function decode(str) {
	return str.replaceAll("&#39;", "'").replaceAll("&quot;", "\"");
}


/**
 * Get the id from a /watch, /shorts or youtu.be URL
 * @param {string} url 
 * @returns {?string}
 */
export function getId(url)
{
	return url.match(/(watch\?v=|youtu.be\/|\/shorts\/)([^\?&]+)/)?.[2];
}

/**
 * Searches something on YouTube.
 * @param {string} q The query
 * @returns {Promise<Array>} A Promise that resolves to an array of results
 */
export async function search(q)
{
	const response = await fetch(SEARCH_URL+encodeURIComponent(q)).then(r => r.json());
	if(response.error)
		throw response.error;
	return get(response.items.map(({id}) => id.videoId).join(","));
}


function parseDuration(ptFormat) { // PT1H23M55S
	const units = [["H", 3600], ["M", 60], ["S", 1]];
	ptFormat = ptFormat.substring(2);
	let duration = 0, value;
	for(const [separator, factor] of units)
	{
		[value, ptFormat] = ptFormat.split(separator);
		if(ptFormat !== undefined)
			duration += value * factor;
		else
			ptFormat = value;
	}
	return duration;
}


/**
 * Converts any t argument (for instance &t=2m30s) into seconds.
 * @param {url} t The video URL or t argument
 * @returns {number}
 */
export function tToSeconds(url)
{
	const t = url.match(/t=([0-9]+h)?([0-9]+m)?([0-9]+s?)?/);
	if(!t) return 0;
	const [, h, m, s] = t;
	let seconds = 0;
	if(h)
		seconds = 3600 * parseInt(h);
	if(m)
		seconds += 60 * parseInt(m);
	if(s)
		seconds += parseInt(s);
	return seconds;
}

/**
 * Returns the given duration in the format mm:ss, or h:mm:ss if the duration is at least an hour.
 * @param {integer} seconds The duration
 * @param {integer} start (optional) the start time.
 * @returns {string} [h:]mm:ss, or if startTime is pecified, [h:]mm:ss->[h:]mm:ss
 */
export function formatDuration(seconds, start)
{
	let minutes = ~~(seconds / 60);
	const hours = ~~(minutes / 60);
	minutes -= hours * 60;
	seconds = seconds % 60;
	const total = `${hours ? `${hours}:` : ""}${minutes < 10 ? `0${minutes}` : minutes}:${seconds < 10 ? `0${seconds}` : seconds}`;
	return start ? `${formatDuration(start)}->${total}` : total;
}

/**
 * Gets information about one or several videos.
 * @param {string} ids The video ids, separated by commas
 * @returns {Promise<Array>} A Promise that resolves to an array of results
 */
export async function get(ids)
{
	const response = await fetch(GET_URL+ids).then(r => r.json());
	if(response.error)
		throw response.error;
	return response.items.map(({id, snippet, contentDetails}) => ({
		id,
		title: decode(snippet.title),
		channel: decode(snippet.channelTitle),
		channelId: snippet.channelId,
		publishedAt: snippet.publishedAt,
		thumbnails: snippet.thumbnails,
		duration: parseDuration(contentDetails.duration),
		ageRestricted: contentDetails.contentRating.ytRating === "ytAgeRestricted",
	}));
}


/**
 * Get a YouTube URL from track data.
 * @param {object} track The track data, as would be returned by get() for instance.
 * @returns {string} a youtu.be URL
 */
export function getURL({ id, startAt, playlist }) {
	startAt = startAt ? `&t=${startAt}` : "";
	playlist = playlist ? `?list=${playlist}` : "";
	return `https://youtu.be/${id}${startAt}${playlist}`;
}


/**
 * Returns the best possible thumbnail out of the given thumbnails.
 * @param {object} thumbnails The thumbnails, as returned by the API.
 * @returns {string|undefined} The URL of the thumbenail with highest possible resolution.
 */
export function getBestRes(thumbs) {
	return thumbs.medium || thumbs.maxres || thumbs.standard || thumbs.high || thumbs.default;
}


/**
 * Gets information about the 25 first elements of the given playlist, excluding private ones.
 * @param {string} id The playlist id
 * @param {boolean} withPlaylistInfo Whether to include the playlist info (title, published date, etc ('snippet' part))
 * @returns {Promise<Array>} A Promise that resolves to an array of results, or rejects if the playlist does not exist
 */
export async function getPlaylist(id, withPlaylistInfo = true)
{
	const response = await fetch(GET_PLAYLIST_URL+id).then(r => r.json());
	if(response.error)
		throw response.error;

	const items = await get(response.items
		.filter(({status: {privacyStatus: p}}) => p === "public" || p === "unlisted")
		.map(({contentDetails: {videoId}}) => videoId)
		.join(",")
	);

	if(withPlaylistInfo)
	{
		const info = await fetch(GET_PLAYLIST_INFO_URL+id).then(r => r.json());
		if(info.error)
			throw info.error;

		items.playlistInfo = info.items[0]?.snippet;
	}

	return items;
}
