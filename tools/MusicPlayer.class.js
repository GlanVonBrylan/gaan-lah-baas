
import { client } from "../bot.js";
client.on("voiceStateUpdate", (oldState, newState) => newState.guild.musicPlayer?.onVoiceStateChange(oldState, newState));


import { execSync } from "node:child_process";
import { Readable } from "node:stream";

export const MAX_DURATION = 130*60; // in seconds

import { VoiceChannel } from "discord.js";
import {
	joinVoiceChannel,
	createAudioPlayer, AudioPlayerStatus,
	createAudioResource, demuxProbe,
} from "@discordjs/voice";
const { Idle: IDLE } = AudioPlayerStatus;

import { create } from "youtube-dl-exec";
// The default import fails because of tinyspawn if there are spaces in the folder path
const ytdl = create("node_modules/youtube-dl-exec/bin/yt-dlp");

import { formatDuration, getBestRes, getURL } from "./youtube.js";


export class FileStreamer
{
	constructor(stream, { name, size, duration }, author) {
		this.name = name;
		this.title = name.substring(0, name.lastIndexOf("."));
		this.duration = duration || 0;
		this.id = `${name}_${size}`;
		this.author = author;
		this.stream = stream;
		this.kill = stream.cancel.bind(stream);
	}

	static probeDuration(path) {
		return +execSync(
			`ffprobe -v error -select_streams a:0 -show_entries stream=duration "${path}"`,
			{encoding: "utf-8"})
			.match(/[0-9]+/)?.[0];
	}
}


export class MusicPlayer
{
	get status() {
		const { connection, current } = this;
		return `playerChannel : ${this.#channel}\n${
		connection ? `Connecté à ${connection.channel}\nActuelle : ${current ? `\`${current.id}\` ${current.title}` : "*aucune*"}\nEn attente : ${this.#queue.length}`
		: "Non connecté"}`;
	}

	constructor(guild) {
		if(guild.musicPlayer)
			return guild.musicPlayer;

		this.guild = guild;
		guild.musicPlayer = this;

		this.player = createAudioPlayer();
		this.player.on("error", err => {
			err.guild = this.guild;
			err.current = this.current;
			error(err);
		});
		this.player.on("stateChange", (_, {status}) => {
			if(status === IDLE && this.connection && !this.skipped)
				this.playNextSong();
		});

		this.playNextSong = this.playNextSong.bind(this); // so we can use it as a callback
	}

	connection = null;
	current = null;
	looping = false;
	#queue = [];
	#channel = null;
	/** @param {VoiceChannel} channel The new channel */
	set channel(channel) { this.#channel = channel; }


	onVoiceStateChange({channel}, {channel: newChannel, id, guild: {members: {me}}}) {
		const { connection } = this;
		// Do not use connection?.channel, if both are undefined this will crash
		if(connection && (
			channel === connection.channel && channel.members.size <= 1 // bot is alone
			|| !newChannel && id === me.id // bot got forcefully disconnected
		))
			this.disconnect();
	}

	sendPlainText(text, thumbnail, onError = Function()) {
		return this.#channel?.send({ embeds: [{ color: 0x5CACEC, description: text, thumbnail }]}).catch(onError);
	}
	sendError(title, err, onError = Function()) {
		console.error(err);
		return this.#channel?.send({ embeds: [{
			color: 0xB81414,
			description: `Oups, erreur en voulant jouer ${title}...`,
			footer: { text: err.message },
		}]}).catch(onError);
	}

	cantConnect = channel =>
		!channel ? "Vous devez être dans un salon vocal."
			: channel.full ? "Ce salon est plein."
			: !channel.speakable ? "Je ne peux pas parler dans ce salon."
			: false;

	connect(channel) {
		if(!(channel instanceof VoiceChannel))
			throw new TypeError("'channel' must be a VoiceChannel");

		if(this.cantConnect(channel))
			return false;

		const connection = this.connection = joinVoiceChannel({
			channelId: channel.id,
			guildId: channel.guild.id,
			adapterCreator: channel.guild.voiceAdapterCreator,
		});
		connection.on("error", error);
		connection.channel = channel;
		connection.subscribe(this.player);

		return true;
	}

	connected() { return this.connection?.channel; }

	get queue() {
		const { connection } = this;
		return connection ? ({ current: this.current, looping: this.looping, queue: this.#queue, channel: connection.channel }) : null;
	}

	addToQueue(songs, atTheBeginning = false, skip = 0) {
		if(!(songs instanceof Array))
			songs = [songs];

		if(!(songs[0] instanceof FileStreamer))
		{
			songs = songs.filter(({duration}) => (duration <= MAX_DURATION));
			if(!songs.length)
				return false;
		}

		if(atTheBeginning)
		{
			if(skip)
				this.#queue.splice(0, skip - 1, ...songs);
			else
				this.#queue.unshift(...songs);
		}
		else
		{
			if(skip > 1)
				this.#queue.splice(0, skip - 1);
			this.#queue.push(...songs);
		}

		if(!this.current || skip)
			this.playNextSong();
		else if(this.current?.download.done)
			this.#preemptDownload();

		return this.#queue.length;
	}

	skip(n = 1, fromQueueOnly = false) {
		if(!Number.isInteger(n) || n < 1)
			throw new TypeError("'n' must be a positive integer");

		if(fromQueueOnly)
			this.#queue.splice(0, n);
		else
		{
			this.skipped = this.current.skipped = true;
			if(n > 1)
				this.#queue.splice(0, n - 1);
			this.player.stop();
			this.playNextSong();
		}
	}

	/**
	 * Removes all duplicates in the current queue.
	 * @returns null if the queue is empty, false if it only has 1 song, the number of duplicates removed otherwise.
	 */
	removeDuplicates() {
		const queue = this.#queue;
		if(!queue?.length)
			return null;
		if(queue.length === 1)
			return false;

		const ids = new Set();
		const newQueue = queue.filter(({id}) => {
			const seen = ids.has(id);
			ids.add(id);
			return !seen;
		});
		const diff = queue.length - newQueue.length;
		if(diff)
		{
			queue.length = 0;
			queue.push(...newQueue);
		}
		return diff;
	}


	/**
	 * Plays the next song in queue.
	 * @returns a Promise that resolves to true if all goes well, with false if the bot was disconnected before it started playing, or an error otherwise.
	 */
	playNextSong()
	{
		if(this.current?.hadError && !this.current.hadAnotherTry)
		{
			this.current.download = null;
			this.current.hadAnotherTry = true;
		}
		else
		{
			if(this.looping)
				this.#queue.push(this.current);
			this.current = this.#queue.shift();
		}

		const { current } = this;
		if(!current)
			return this.stop(), false;

		const isFile = current instanceof FileStreamer;

		this.connection.dlProcess?.kill();
		this.connection.dlProcess = null;
		const { id, title, ageRestricted } = current;
		if(ageRestricted)
		{
			const cantPlay = this.sendPlainText(`🔞 Impossible de lire [${title}](https://youtu.be/${id}), cette vidéo est soumise à une limite d’âge.`);
			cantPlay.then(this.playNextSong);
			return cantPlay;
		}

		this.lastPlayStart = Date.now();

		return new Promise(async (resolve) => {
			const { process, stream, resource } = await this.#download(current);
			this.connection.dlProcess = process;
			if(isFile)
				this.#preemptDownload();
			else
				process.then(() => {
					current.download.done = true;
					this.#preemptDownload();
				});

			let confirmMsg, hadError;

			const onError = (err) => {
				if(current.skipped || current !== this.current)
					return resolve(true);

				console.error(err);
				if(!current.hadError && Date.now() - this.lastPlayStart < 10_000)
					current.hadError = hadError = true;
				else
					this.sendError(title, err);

				confirmMsg?.delete().catch(Function());
				this.playNextSong();
				resolve(err);
			}

			if(stream.errored)
				return onError(stream.errored);

			stream.on("error", onError);

			stream.on("end", () => resolve(true));

			if(!this.current) // In the case the bot is disconnected while ytdl loads
				return resolve(false);

			this.player.play(resource);
			this.skipped = false;
			current.startedPlaying = ~~(Date.now() / 1000);

			const { duration, channel, thumbnails } = current;

			const msg = isFile ? this.sendPlainText(
				`🎶 **${title}** (${formatDuration(duration)})\n*(envoyé par ${current.author})*`,
			) : this.sendPlainText(
				`🎶 [${title}](${getURL(current)}) (${formatDuration(duration)})\n${channel}`,
				getBestRes(thumbnails),
			);
			msg.then(msg => {
				if(hadError)
					msg.delete().catch(Function());
				else
					confirmMsg = msg;
			});
		});
	}
	#preemptDownload() {
		const [next] = this.#queue;
		if(next)
			this.#download(next);
	}
	async #download(item) {
		if(item.download)
			return item.download;

		const info = {};
		const isFile = item instanceof FileStreamer;
		const { id, title, startAt = 0 } = item;
		const metadata = { title };
		if(isFile)
		{
			info.process = item;
			info.done = true;
		}
		else
		{
			metadata.url = `https://youtu.be/${id}`;
			const options = { format: "bestaudio", o: "-" };
			if(startAt)
				options.downloadSections = `*${startAt}-inf`;
			info.process = ytdl.exec(id, options);
			info.process.catch(Function()); // interrupting the download always throws
		}
		
		const stream = info.stream = isFile
			? Readable.fromWeb(item.stream)
			: info.process.stdout;
		const { type: inputType } = await demuxProbe(stream);
		info.resource = createAudioResource(stream, { metadata, inputType });
		return item.download = info;
	}

	shuffle() { this.#queue.shuffle(); }


	stop() {
		try {
			this.connection.dlProcess?.kill();
		} catch(err) {
			if(!(err instanceof TypeError && err.message.includes("Invalid state")))
				error(err);
		}
		this.connection.dlProcess = null;
		this.player.stop();
		this.current = null;
		this.looping = false;
		this.#queue.length = 0;
	}

	disconnect() {
		if(!this.connection)
			return false;

		this.stop();
		this.connection.destroy();
		this.connection = null;
		return true;
	}
}


Object.defineProperty(MusicPlayer, "MAX_DURATION", { value: MAX_DURATION });

export default MusicPlayer;


export function connect(inter)
{
	const { musicPlayer: player } = inter.guild;
	const { channel } = inter.member.voice;
	if(channel !== player.connected()?.channel)
	{
		const error = player.cantConnect(channel);
		if(error)
			return { error };

		player.connect(channel);
	}
	return player;
}