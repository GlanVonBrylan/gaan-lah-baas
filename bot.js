
import { Client, GatewayIntentBits as INTENTS } from "discord.js";
import { require } from "./utils.js";
export const auth = require("auth.json");

export const client = new Client({
	intents: [
		INTENTS.Guilds,
		INTENTS.GuildVoiceStates,
	],
});

export var myself;
export var master;

export function sendToMaster(msg, onError = error) { return master.send(msg).catch(onError); }

import "./error.js";
import loadCommands from "@brylan/djs-commands";
import { handleInteraction as componentInteraction } from "./components.js";

client.on("error", error);
client.login(auth.token);

client.once("ready", async () => {
	myself = client.user;
	master = await client.users.fetch(auth.master);
	console.log(`Connecté en tant que ${client.user.tag} !`);

	const { default: MusicPlayer } = await import("./tools/MusicPlayer.class.js");
	client.on("interactionCreate", interaction => {
		if(!interaction.guild.musicPlayer)
			new MusicPlayer(interaction.guild);
	
		if(interaction.isMessageComponent())
			return componentInteraction(interaction);
	
		if(interaction.commandName !== "owner")
			interaction.guild.musicPlayer.channel = interaction.channel;
	});

	loadCommands(client, {
		debug: auth.DEBUG_MODE,
		ownerServer: auth.adminServer,
		makeEnumsGlobal: true,
	});
});
